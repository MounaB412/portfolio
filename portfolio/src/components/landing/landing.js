import React from 'react';
import './landing.css';
import {FormattedMessage} from 'react-intl';

const Landing = () => (
  <div className="Landing">
    <h1>Mouna Belaïd</h1>
    <p className="tagline">
    <FormattedMessage
         id = "landing.tagline"
       /> <br/>
    <FormattedMessage
        id = "landing.job"
    />
    </p>
  </div>
);

Landing.propTypes = {};

Landing.defaultProps = {};

export default Landing;
